@extends('layout.master')
@section('title', 'Manage Designation')
@section('main-content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Manage Designation</h1>
        <div class="card mb-4">
            <div class="card-body">
                @if(isset($designations))
                    <form action="{{ route('designations.update', $designations->id) }}" method="post" enctype="multipart/form-data">
                        @method('put')
                @else
                    <form action="{{ url('designations') }}" method="post" enctype="multipart/form-data">
                @endif 
                    @csrf
                    <div class="form-group">
                        <label>Designation Name <span class="text-danger">*</span></label>
                        <input type="text" name="designation_name" id="designation_name" class="form-control" placeholder="Enter Designation Name" value="{{ old('designation_name', $designations->designation_name ?? '') }}">
                        <span class="text-danger">@error('designation_name') {{ $message }} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Description </label>
                        <input type="text" name="description" id="description" class="form-control" placeholder="Enter Description" value="{{ old('description', $designations->description ?? '') }}">
                        <span class="text-danger">@error('description') {{ $message }} @enderror</span>
                    </div>
                    <input type="submit" value="Save" class="btn btn-success">
                </form>
            </div>
        </div>
    </div>
</main>
@endsection
