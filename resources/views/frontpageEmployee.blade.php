<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Upskill - @yield('title')</title>
    <link href="{{ asset('public/dist/css/styles.css') }}" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css">
</head>

<body class="sb-nav-fixed">
<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
    <a class="navbar-brand" href="{{ url('admin') }}">Frontend</a>
    <!-- Navbar Search-->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
        <!-- <div class="input-group">
            <input class="form-control" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
            <div class="input-group-append">
                <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
            </div>
        </div> -->
    </form>
    <!-- Navbar-->
    <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <a href="{{ url('login') }}" class="dropdown-item"><i class="fas fa-user"></i> Login</a>
            </div>
        </li>
    </ul>
</nav>
<main class="mt-5">
    <div class="container-fluid mt-5">
        <h1 class="mt-4">view Employee</h1>
        <div class="row">
            <div class="add-btn col-md-11">
            </div>
            <div class="add-btn col-md-1">
                <!-- <a href="{{ route('employees.create') }}"><button class="btn btn-dark pull-right">ADD <i class="fa fa-plus"></i></button></a>     -->
            </div>
        </div>
        <div class="card mb-4 mt-3">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>email</th>
                                <th>company</th>
                                <th>Designation</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            @foreach($employees as $item)
                            <tr>
                                <td>{{ $item->firstname }}</td>
                                <td>{{ $item->lastname }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $item->company_name }}</td>
                                <td>{{ $item->designation_name ?? "" }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('public/dist/js/scripts.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('public/dist/assets/demo/chart-area-demo.js') }}"></script>
    <script src="{{ asset('public/dist/assets/demo/chart-bar-demo.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('public/dist/assets/demo/datatables-demo.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script>
        @if(session('status'))
        swal({
            title: '{{ session('
            statusCode ') }} ',
            text: '{{ session('
            status ') }}',
            icon: "success",
            button: "okay",
        });
        @endif
        $(document).ready(() => {
            $("#logo").change(function() {
                const file = this.files[0];
                if (file) {
                    let reader = new FileReader();
                    reader.onload = function(event) {
                        $("#preview")
                            .attr("src", event.target.result);
                    };
                    reader.readAsDataURL(file);
                }
            });
        });
        document.querySelectorAll('.delete').forEach(button => {
            button.addEventListener('click', function(e) {
                e.preventDefault(); // Prevent the form from submitting immediately
                // Ask for confirmation before deleting
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this Record!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            let formId = this.closest('form').getAttribute('id');
                            console.log(formId)

                            document.getElementById(formId).submit();
                            swal("Poof! Your Recrod has been deleted!", {
                                icon: "success",
                            });
                        } else {
                            swal("Your Record is safe!");
                        }
                    });
            });
        });


        //Ajax call of employees status
        $('input[type="checkbox"]').change(function() {
            var employeeId = $(this).data('id');
            var status = $(this).prop('checked') ? 1 : 0;
            console.log(status);
            console.log("hheheh");
            $.ajax({
                url: '/employees/' + employeeId + '/status',
                type: 'POST',
                data: {
                    _method: 'PUT',
                    _token: '{{ csrf_token() }}',
                    status: status
                },
                success: function(response) {
                    swal({
                        title: response.message,
                        text: response.message,
                        icon: "success",
                        button: "okay",
                    });
                },
                error: function(xhr) {
                    swal({
                        title: "status does not updated something went wrong",
                        text: "status does not updated something went wrong",
                        icon: "danger",
                        button: "okay",
                    });
                }
            });
        });
    </script>
</body>
</html>
