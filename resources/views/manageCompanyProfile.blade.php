@extends('layout.compantadminmaster')
@section('title', 'Manage Profile')
@section('main-content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Manage Company</h1>
        <div class="card mb-4">
            <div class="card-body">
                @if(isset($user))
                    <form action="{{ route('profile.update', $company->id) }}" method="post" enctype="multipart/form-data">
                        @method('put')
                @endif 
                    @csrf
                    <div class="form-group">
                            <label>Name <span class="text-danger">*</span></label>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Enter Name" value="{{ old('name', $company->name ?? '') }}">
                            <span class="text-danger">@error('name') {{ $message }} @enderror</span>
                        </div>
                        <div class="form-group">
                            <label>Email <span class="text-danger">*</span></label>
                            <input type="email" name="email" id="email" class="form-control" placeholder="abc@gmail.com" value="{{ old('email', $company->email ?? '') }}">
                            <span class="text-danger">@error('email') {{ $message }} @enderror</span>
                        </div>
                        <fieldset style="border: 1px solid #ccc; padding: 20px; margin-bottom: 20px;">
                            <legend style="font-weight: bold; padding: 0 10px;">Login Detail Section</legend>

                            <div class="form-group">
                                <label for="loginEmail">Login Email <span class="text-danger">*</span></label>
                                <input type="email" name="loginEmail" id="loginEmail" class="form-control" placeholder="abc@gmail.com" value="{{ old('loginEmail', $company->loginEmail ?? '') }}">
                                <span class="text-danger">@error('loginEmail') {{ $message }} @enderror</span>
                            </div>

                            <div class="form-group">
                                <label for="password">Password <span class="text-danger">*</span></label>
                                <input type="password" name="password" id="password" class="form-control" placeholder="Enter password">
                                <span class="text-danger">@error('password') {{ $message }} @enderror</span>
                            </div>
                        </fieldset>

                        <div class="form-group">
                            <label>Website <span class="text-danger">*</span></label>
                            <input type="text" name="website" id="website" class="form-control" placeholder="www.abc.com" value="{{ old('website', $company->website ?? '') }}">
                            <span class="text-danger">@error('website') {{ $message }} @enderror</span>
                        </div>
                        <div class="form-group">
                            <label>Contact Number<span class="text-danger">*</span></label>
                            <input type="number" name="contactNumber" id="contactNumber" class="form-control" placeholder="1234567890" value="{{ old('contactNumber', $company->contactNumber ?? '') }}">
                            <span class="text-danger">@error('contactNumber') {{ $message }} @enderror</span>
                        </div>
                        <div class="form-group">
                            <label>Company Details</label>
                            <textarea name="companyDetails" id="companyDetails" class="form-control" placeholder="Enter company details">{{ old('companyDetails', $company->companyDetails ?? '') }}</textarea>
                            <span class="text-danger">@error('companyDetails') {{ $message }} @enderror</span>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-1">Upload Logo <span class="text-danger">*</span></label>
                                <input type="file" name="logo" id="logo" class="form-control col-md-3">
                                @if (isset($company))
                                <input name="oldlogo" type="hidden" value="{{ $company->logo }}">
                                <img src="{{ asset('public/storage/company_logos/' . $company->logo) }}" id="preview" alt="img" height="100px" width="40px" class="col-sm-1 border border-dark ml-4">
                                @else
                                <img src="{{ asset('public/nopreview.png') }}" alt="img" id="preview" height="100px" width="40px" class="col-sm-1 border border-dark ml-4">
                                @endif
                            </div>
                            <span class="text-danger">@error('logo') {{ $message }} @enderror</span>
                        </div>
                        <input type="submit" value="Save" class="btn btn-success">
                        </form>
            </div>
        </div>
    </div>
</main>
@endsection