<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $company = Company::all();
        return view('viewCompany')->with('company', $company);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/manageCompany');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'name' =>  'required|max:100',
        //     'email' => 'required|email|unique:companies,email',
        //     'logo' => 'required|dimensions:min_width=100,min_height=100|file|mimes:jpg,png|min:100|max:2040',
        //     'website' => 'required'
        // ],
        // [
        //     'logo.required' => "you have to choose company logo it required.",
        //     'logo.dimensions' => "you have to choose image minimum 100 x 100 dimension.",
        //     'logo.min' => 'The logo must be at least 100KB.',
        //     'logo.max' => 'The logo may not be greater than 200MB.',
        //     'name.max' => 'The firstname may not be greater than 100 characters.',
        // ]);
        $request->validate([
            'name' => 'required|max:100',
            'email' => 'required|email|unique:company,email',
            'loginEmail' => 'required|email|unique:company,loginEmail',
            'password' => 'required|max:100',
            'logo' => 'required|mimes:jpg,png',
            'website' => 'required',
            'contactNumber' => ['required', 'string', 'size:10'],
        ], [
            'logo.required' => 'You have to choose a company logo; it is required.',
           // 'logo.dimensions' => 'The logo must be at least 100 x 100 pixels in dimensions.',
            'logo.mimes' => 'The logo must be a JPG or PNG file.',
            //'logo.between' => 'The logo must be between 100KB and 2MB in size.',
            'password.required' => 'Password is required please and password',
            'password.max' => 'The password may not be greater than 100 characters',
            'name.max' => 'The name may not be greater than 100 characters.',
            'contactNumber.required' => 'Contact Number it is required',
            'contactNumber.size' => 'Contact Number must be 10 digit',
        ]);
        $input = $request->all();
        if($request->file('logo')){
            $file= $request->file('logo');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file-> move(storage_path('app/public/company_logos/'), $filename);
        }
        $input['logo'] = $filename;
        $company = Company::create($input);
        // create user
        $userInput = [
            'name' => $input['name'],
            'email' => $input['loginEmail'],
            'password' => bcrypt($input['password']), // Hash the password
            'company_id' => $company->id,
            // You might want to adjust this based on your user table structure
        ];
        $user = User::create($userInput);

        
        // Assign the 'company-admin' role to the user
        $role = Role::where('name', 'company-admin')->first(); // Assuming 'company-admin' is the role name
        $user->roles()->attach($role);

        
        Session::put('statusCode', 'success');
        return redirect()->route('companies.index')->with('status', 'Record Addedd Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('companies.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        return  view('manageCompany')->with('company', $company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::find($id);
        $request->validate([
            'name' =>  'required|max:100',
            'email' => 'required|email|unique:company,email,' . $id . ',id',
            'logo' => 'dimensions:min_width=100,min_height=100',
            'website' => 'required'
        ],
        [
            'logo.dimensions' => "you have to choose image minimum 100 x 100 dimension.",
            'name.max' => 'The firstname may not be greater than 100 characters.',
        ]);
        $filename = null;
        if($request->file('logo') !== null){
            $file= $request->file('logo');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file-> move(storage_path('app/public/company_logos/'), $filename);
            File::delete(storage_path('app/public/company_logos/' . $request->oldlogo));
        } else {
            $filename = $request->oldlogo;
        }
        $company->name = $request->name;
        $company->email = $request->email;
        $company->website = $request->website;
        $company->logo = $filename;
        $company->save();
        return redirect()->route('companies.index')->with('status', 'Record Updated Successfully!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        File::delete(storage_path('app/public/company_logos/' . $company->logo));
        $company->delete();
        Session::put('statusCode', 'success');
        return redirect()->route('companies.index')->with('status', 'Record Deleted Successfully!');
    }
}
