<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Models\Employee;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $company = Company::all();
        return view('frontpage')->with('company', $company);
    }

    public function show($id)
    {
        $employees = DB::table('employee')
            ->join('company', 'employee.company_id', '=', 'company.id')
            ->join('designations', 'employee.designation_id', '=', 'designations.id')
            ->where('employee.company_id', $id)
            ->select('employee.*', 'company.name as company_name', 'designations.designation_name as designation_name')
            ->get();

        return view('frontpageEmployee')->with('employees', $employees);
    }
}
