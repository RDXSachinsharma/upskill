<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;

class CompanyProfileController extends Controller
{
    //
    public function index()
    {
        $user = Auth::user();
        $company = Company::find($user->company_id);
        return view('manageCompanyProfile')->with('company', $company);
        
    }
    public function update(Request $request, $id)
    {
        dd('ddd');
        $company = Company::find($id);
        $request->validate([
            'name' =>  'required|max:100',
            'email' => 'required|email|unique:company,email,' . $id . ',id',
            'logo' => 'dimensions:min_width=100,min_height=100',
            'website' => 'required'
        ],
        [
            'logo.dimensions' => "you have to choose image minimum 100 x 100 dimension.",
            'name.max' => 'The firstname may not be greater than 100 characters.',
        ]);
        $filename = null;
        if($request->file('logo') !== null){
            $file= $request->file('logo');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file-> move(storage_path('app/public/company_logos/'), $filename);
            File::delete(storage_path('app/public/company_logos/' . $request->oldlogo));
        } else {
            $filename = $request->oldlogo;
        }
        $company->name = $request->name;
        $company->email = $request->email;
        $company->website = $request->website;
        $company->logo = $filename;
        $company->save();
        return redirect()->route('company.profile')->with('status', 'Record Updated Successfully!');

    }
}
