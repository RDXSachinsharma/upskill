<?php

namespace App\Http\Controllers;
use App\Models\Designation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $designations = Designation::all();
        return view('viewDesignations')->with('designations', $designations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        return view('manageDesignations');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'designation_name' => 'required|string|max:255',
            'description' => 'nullable|string',
        ]);

        Designation::create($request->all());
        return redirect()->route('designations.index')->with('status', 'Record Addedd Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $designations = Designation::find($id);
        return view('manageDesignations')->with('designations', $designations);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $designations = Designation::find($id);
        $request->validate([
            'designation_name' => 'required|string|max:255',
            'description' => 'nullable|string',
        ]);
        $designations->designation_name = $request->designation_name;
        $designations->description = $request->description;
        $designations->save();
        Session::put('statusCode', 'success');
        return redirect()->route('designations.index')->with('status', 'Record Updated Successfully!');
    }   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $designations = Designation::find($id);
        $designations->delete();
        Session::put('statusCode', 'success');
        return redirect()->route('designations.index')->with('status', 'Record Deleted Successfully!');
    }
}
