<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Mail\NotifyMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;



class EmployeeapiController extends Controller
{
    // listing all employess
    public function index()
    {
        //$employees = Employee::join('company', 'employee.company_id', '=', 'company.id')->select('employee.*', 'company.name')->get();
        //$employees = Employee::with('getCompany')->get();
        $employees = Employee::with('company')->get();
        return response()->json($employees);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'firstname' =>  'required',
            'lastname' => 'required',
            'email' => 'required|unique:employee',
            'company_id' => 'required'
        ]);
        $input = $request->all();
        Employee::create($input);
        Mail::to($input['email'])->send(new NotifyMail($input['firstname']));
        return response()->json(['message' => 'Record inserted successfully'], 201);
    }

    public function update(Request $request, $id)
    {
        //
        $employee = Employee::find($id);
        // $request->validate([
        //     'firstname' =>  'required',
        //     'lastname' => 'required',
        //     'email' => 'required|unique:employee,email,' . $id . ',id',
        //     'company_id' => 'required'
        // ]);
        $employee->firstname = $request->firstname;
        $employee->lastname = $request->lastname;
        $employee->email = $request->email;
        $employee->company_id = $request->company_id;
        $employee->save();
       return response()->json(['message' => 'Record updated successfully'], 201);
    }
    public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee->delete();
        
        return response()->json(['message' => 'Record deleted successfully'], 201);
    }
}
