<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class CompanyapiController extends Controller
{
    //
    public function index()
    {
        $company = Company::all();
        return response()->json($company);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' =>  'required',
            'email' => 'required|unique:company',
            'logo' => 'required|dimensions:min_width=100,min_height=100',
            'website' => 'required'
        ],
        [
            'logo.required' => "you have to choose company logo it required.",
            'logo.dimensions' => "you have to choose image minimum 100 x 100 dimension."
        ]);
        $input = $request->all();
        if($request->file('logo')){
            $file= $request->file('logo');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file-> move(storage_path('app/public/company_logos/'), $filename);
        }
        $input['logo'] = $filename;
        Company::create($input);
        return response()->json(['message' => 'Record inserted successfully'], 201);
    }

    public function update(Request $request, $id)
    {
        $company = Company::find($id);
        $request->validate([
            'name' =>  'required',
            'email' => 'required|unique:company,email,' . $id . ',id',
            'logo' => 'dimensions:min_width=100,min_height=100',
            'website' => 'required'
        ],
        [
            'logo.dimensions' => "you have to choose image minimum 100 x 100 dimension."
        ]);
        $filename = null;
        if($request->file('logo') !== null){
            $file= $request->file('logo');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file-> move(storage_path('app/public/company_logos/'), $filename);
            File::delete(storage_path('app/public/company_logos/' . $request->oldlogo));
        } else {
            $filename = $request->oldlogo;
        }
        $company->name = $request->name;
        $company->email = $request->email;
        $company->website = $request->website;
        $company->logo = $filename;
        $company->save();
        return response()->json(['message' => 'Record updated successfully'], 201);

    }

    public function destroy($id)
    {
        $company = Company::find($id);
        File::delete(storage_path('app/public/company_logos/' . $company->logo));
        $company->delete();
        return response()->json(['message' => 'Record deleted successfully'], 201);
    }
}
