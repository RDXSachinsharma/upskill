<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\CompanyapiController;
use App\Http\Controllers\API\EmployeeapiController;
use Illuminate\Http\Request;
use App\Http\Middleware\AuthenticateSanctum;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/login', [AuthController::class, 'login']);
Route::middleware(['auth:sanctum'])->group(function () {
    Route::resource('/companies', CompanyapiController::class);
    Route::resource('/employees', EmployeeapiController::class);
});
 //http://upskill.sachins.sharma.radixusers2.com/api/employees/1?_method=PUT